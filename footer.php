<?php wp_footer(); ?>

<div class="w-full bg-black pt-4">
    <div class="flex justify-center">
        <div class="text-center">
            <span class="text-4xl" style="color: rgba(73, 234, 121, 1)">AWORIA</span><br>
            <a href="#" style="color: rgba(73, 234, 121, 1)">Regulamin</a>
            <a href="#" style="color: rgba(73, 234, 121, 1)">Polityka cookies</a><br>
            <span class="text-white"> ©2018 Aesir</span>
        </div>
    </div>
</div>

</body>

</html>
