import Vue from 'vue';
import axios from 'axios';

import VueLodash from 'vue-lodash'
import lodash from 'lodash'
Vue.use(VueLodash, {name: 'custom', lodash: lodash});

require('./bootstrap');
require('./my_hacks.js');

import './assets/css/styles.css';

import router from './router';
import store from './store/index';

import Notice from './components/Partials/Notice.vue';

console.log("by Maciej Kułak 2020");

/* Global components */
Vue.component('modal', require('./components/Partials/Modal'));

Vue.mixin({
  methods: {
    $openModal(tag){
      this.$store.commit('modal/openModal', tag);
    },
    $setButton(dest, desc){
      this.$store.commit('modal/setButton', [dest, desc]);
    }
  },
  data() {
    return {
      axios: axios,
    }
  }
})

import App from './App.vue';

Vue.component("notice", Notice);

new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App),
  
});
