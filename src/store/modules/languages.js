import Vue from 'vue'
import axios from 'axios'

import env from '../../../env'

const state = {
    languages: [],
    translations: [],
    downloaded: false,

    selectedInput: 0,
    selectedOutput: []
}

const mutations = {
    updateLanguages(state, payload){
        state.languages = payload;
        Vue.set(state, 'languages', payload);
    },

    updateTranslations(state, payload){
        state.translations = payload;
        Vue.set(state, 'translations', payload);
    },

    changeInput(state, payload){
        state.selectedInput = payload;
        Vue.set(state, 'selectedInput', payload);

        state.selectedOutput = [];
        Vue.set(state, 'selectedOutput', []);
    },

    changeToDownloaded(state){
        state.downloaded = true;
        Vue.set(state, 'downloaded', true);
    },

    addOutput(state, payload){
        if(!state.selectedOutput.includes(payload) && payload != 0){
            state.selectedOutput.push(payload);
            Vue.set(state, 'selectedOutput', state.selectedOutput);
        }
    },
    deleteOutput(state, payload){
        let index = state.selectedOutput.indexOf(payload);
        state.selectedOutput.splice(index, 1);
        Vue.set(state, 'selectedOutput', state.selectedOutput);
    }
}

const getters = {
    getNameByID: (state, getters) => (id) => {
        let result;
        state.languages.forEach(lang => {
            if(lang.id == id){
                result = lang.title.rendered;
            }
        });
        return result;
    },

    getSlugByID: (state, getters) => (id) => {
        let result;
        state.languages.forEach(lang => {
            if(lang.id == id){
                result = lang.slug;
            }
        });
        return result;
    },

    getPrice: (state) => (id1, id2) => {
        let result = false;
        let resultLang = {};
        for(let lang of state.languages) {
            if(lang.id == id1){
                lang.languages.forEach((lang2) => {
                    if(lang2.ID == id2){
                        result = true;
                        resultLang = lang2;
                    }   
                })
            }
        }
        if(result){
            return resultLang;
        } else {
            return result;  
        }
    }
}

const actions = {
    getLanguages({commit, dispatch, getters, state}){
        if(state.downloaded == false){
            axios.get(env.api + 'language')
                .then((response) => {
                    commit('updateLanguages', response.data);
                    commit('changeToDownloaded');
                    return response;
                }).then((response) => {
                    let data = _.clone(response.data);
                    let newTranslations = {};
                    
                    data.forEach(lang => {
                        let temp = [];
                        lang.languages.forEach((prop) => {
                            temp.push(_.clone(prop.ID));
                        });
                        newTranslations[lang.id] = _.clone(temp);
                        commit('updateTranslations', _.clone(newTranslations));
                    });
                })
        }
    },
}

const namespaced = true;

export const languages = {
    state, mutations, getters, actions, namespaced
}