import axios from 'axios'
//import WooCommerceRestApi from "@woocommerce/woocommerce-rest-api";
import env from '../../../env.js';

const axiosInstance = axios.create({
    baseURL: env.url
})

const namespaced = false;

const state = {
    sendedSuccesfully: false,
}


const mutations = {
    responded(state){
        state.sendedSuccesfully = true;
    }
}
const actions = {

    send({state, rootState, rootGetters, commit }){
        var languageInput = rootGetters["languages/getNameByID"](rootState.languages.selectedInput);
        var languageOutput = rootState.languages.selectedOutput.map(id => {
            return rootGetters["languages/getNameByID"](id);
        });


        axiosInstance.post('wp-admin/admin-ajax.php/?action=zlecenie_handler', {
            mode: rootState.wycena.mode,

            /* form */
            termSelected: rootState.form.termSelected,
            transSpecial: rootState.form.transSpecial,
            transType: rootState.form.transType,
            text: rootState.form.text,
            comment: rootState.form.commentForTrans,
            cost: rootState.form.cost,
            
            /* language */
            languageInput: languageInput,
            languageOutput: languageOutput,

            /* post */
            clientEmail: rootState.post.clientEmail,
            clientPhone: rootState.post.clientPhone,

            bill: rootState.post.bill,
            billValues: rootState.post.billValues,
            delivery: rootState.post.delivery,
            deliveryType: rootState.post.deliveryType,
            postType: rootState.post.postType,


            

        }, { headers: { 'Content-Type': 'application/json;charset=UTF-8', "Access-Control-Allow-Origin": "*" } })
        
        .then(response => {
            commit("modal/addLogFullscreen", "Dane dotyczące tłumaczenia trafiły na serwer");

            commit('form/setIdJob', response.data);

            axiosInstance.post('wp-admin/admin-ajax.php/?action=file_handler&id='+response.data, rootState.form.formData)
                .then(response => {
                    commit("modal/addLogFullscreen", "Wszystkie pliki trafiły na serwer");
                    if(rootState.form.cost == 0){
                        setTimeout(() => {
                            commit("modal/stopLoading");
                        }, 500);
                        commit("modal/addLogFullscreen", "Płatność wynosi 0zł, wypełnij odpowiednio formularz");
                    } else if(typeof rootState.form.cost == "number") {
                        commit("modal/addLogFullscreen", "Łączenie z pośrednikiem płatności dotpay");
                        axiosInstance.post(
                            env.url+'wp-admin/admin-ajax.php/?action=payment_handler&id='+rootState.form.idJob+
                            "&cost="+rootState.form.cost
                        , {
                            cost: _.clone(rootState.form.cost).toString()
                        },
                        
                        { headers: { 'Content-Type': 'application/json;charset=UTF-8', "Access-Control-Allow-Origin": "*" } })
                            .then(response => {
                                commit("modal/addLogFullscreen", "Wygenerowano link do płatności");
                                commit("modal/addPayment", response.data.link);
                                commit("modal/stopLoading");
                            })

                    } else if(typeof rootState.form.cost == "string"){
                        commit("modal/addLogFullscreen", "Wyceną zlecenia zajmie się tłumacz");
                        setTimeout(() => {
                            commit("modal/stopLoading");
                        }, 500);
                    }
                    

                })
        })
    }
}

export const send = {
    namespaced, actions, state, mutations
}