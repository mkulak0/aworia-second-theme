import Vue from 'vue'
import axios from 'axios'

import env from '../../../env.js';


var typeToMultipiler = {
    'slow':1,
    'medium': 1.3,
    'immediate': 1.6,
    'extreme': 2
}

var transSpecialToMultipiler = {
    'no': 1,
    'yes': 1.2,
    'unkown': false
}

var calculateCost = (transType, transSpecial, termSelected, text, fileList, costMultipiler) => {
    let price = 0;
    let termMultipiler = typeToMultipiler[termSelected];
    let transSpecialMultipiler = transSpecialToMultipiler[transSpecial];

    if(text.length != 0){
        if(transType === 'usual' && costMultipiler.price != undefined){
            price += costMultipiler.price*termMultipiler*transSpecialMultipiler;
            if(text.length - 1500 > 0){
                price += (costMultipiler.price*termMultipiler*transSpecialMultipiler) * (Math.ceil((text.length - 1500)/750)) 
            }
        } else if (transType === 'swear' && costMultipiler.swear != undefined){
            price += costMultipiler.swear*termMultipiler*transSpecialMultipiler;
            if(text.length > 1125 > 0){
                price += (costMultipiler.swear*termMultipiler*transSpecialMultipiler) * (Math.ceil((text.length - 1125)/1125)) 
            }
        }
    }


    return price;
}

const state = {
    text: "",
    formData: new FormData(),
    fileList: {},

    transType: 'none',
    transSpecial: 'none',

    termSelected: 'slow',
    commentForTrans: '',

    cost: "Oczekuję na dane",

    idJob: 0,

    statute: false,
    rodo: false
}

const mutations = {
    updateText(state, payload){
        state.text = payload;
    },

    setTransType(state, payload){
        state.transType = payload;
    },

    setTransSpecial(state, payload){
        state.transSpecial = payload;
    },

    setIdJob(state, payload){
        state.idJob = payload;
    },

    setCost(state, payload){
        state.cost = payload;
    },
    
    internalFileList(state){
        var output = {};
        for(var pair of state.formData.entries()){
            output[pair[0]] = pair[1];
        }
        state.fileList = output;
        state.cost = "Wycena tłumacza";
    },

    termSelect(state, payload){
        state.termSelected = payload;
    },

    updateCommentForTrans(state, payload){
        state.commentForTrans = payload;
    },

    internalDeleteFile(state, payload){
        state.formData.delete(payload);

    },

    setStatute(state, payload){
        state.statute = payload;
    },

    setRodo(state, payload){
        state.rodo = payload;
    },
}

const actions = {
    addFile({ state, commit, dispatch }, payload){
        let randomNumbers = window.crypto.getRandomValues(new Uint32Array(1))[0];

        state.formData.append('file-' + randomNumbers, payload);

        dispatch('updatePrice');

        commit('internalFileList');
    },
    deleteFile({state, commit, dispatch}, payload){
        if(state.fileList[payload] !== undefined){
            commit('internalDeleteFile', payload)
            commit('internalFileList');
        }
        dispatch('updatePrice');

    },


    updatePrice({state, commit, rootState, dispatch, rootGetters}, payload){
        let cost = 0;
        let next = true;

        
        rootState.languages.selectedOutput.forEach(element => {
            if(next){
                let costMultipile = rootGetters['languages/getPrice'](rootState.languages.selectedInput, element);
                let result = calculateCost(state.transType, state.transSpecial, state.termSelected, state.text, state.fileList, costMultipile);
                
                if(result !== false && result !== undefined){
                    cost += result;
                } else if(result === false){
                    cost = "Brak tłumaczenia przysięgłego";
                    next = false;
                } else if(result === undefined || result === NaN){
                    cost = "Oczekuję na dane";
                    next = false;
                }
            }
            
        });
        if(Object.keys(state.fileList).length != 0){
            next = false;
            cost = "Wycena tłumacza";
        }
        if(isNaN(cost) || cost == 0){
            commit('setCost', "Oczekuję na dane");
        } else {
            commit('setCost', cost);
        }
           
    }
}

const getters = {
    fileList: (state) => () => {
        return state.fileList;
    },
    fileListLength: (state) => {
        return Object.size(state.fileList);
    }
}

const namespaced = true;

export const form = {
    namespaced, state, mutations, actions, getters
}