import Vue from 'vue'

const state = {
    mode: "blank"
}

const getters = {
    actualMode: state =>  state.mode
}

const mutations = {
    changeMode(state, payload){
        state.mode = payload;
        Vue.set(state, 'mode', payload);
    }
}

const namespaced = true;

export const wycena = {
    state,
    getters,
    mutations,
    namespaced
  }
  