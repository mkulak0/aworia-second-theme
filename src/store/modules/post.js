import Vue from 'vue'

const namespaced = true;

const state = {
    postType: 'none', // company/private

    clientPhone: '',
    clientEmail: '',
    clientEmailCheck: '',

    bill: false,

    delivery: {
        byEmail: false,
        byPrivate: false,
        byCompany: false,

        name: '',
        address: '',
        postCode:  '',
        city: '',
        country: 'Polska',
        phone: ''
    },

    billValues: {
        name: '',
        address: '',
        postCode:  '',
        city: '',
        country: 'Polska',
        phone: '',

        ID: '',
    },

    deliveryType: 'economy',

}

const getters = {

}

const mutations = {
    setPostType(state, payload){
        state.postType = payload;

        if(payload == 'company'){
            state.bill = true;
        }
    },

    setClientPhone(state, payload){
        state.clientPhone = payload;
    },
    setClientEmail(state, payload){
        state.clientEmail = payload;
    },
    setBill(state, payload){
        state.bill = payload;
    },

    setClientEmailCheck(state, payload){
        state.clientEmailCheck = payload;
    },

    deliveryChange(state, payload){
        state.delivery[payload.prop] = payload.value;
        if(payload.prop == 'byPrivate'){
            state.delivery.byCompany = false;

        } else if (payload.prop == 'byCompany'){
            state.delivery.byPrivate = false;
        }
    },
    billChange(state, payload){
        state.billValues[payload.prop] = payload.value;
    },

    setDeliveryType(state, payload){
        state.deliveryType = payload;
    }

}

export const post = {
    namespaced, state, mutations, getters
}