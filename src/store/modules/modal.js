import Vue from 'vue'

const texts = {
        'loadTextOrFiles' : "Tę opcję należy wybrać jeżeli treść tłumaczenia jest wpisywana ręcznie, kopiowana i wklejana albo ma postać pliku z tekstem.",
        'sendByPost' : "Tę opcję należy wybrać jeżeli treść tłumaczenia znajdują się na dowolnym nośniku informacji (może to być pendrive lub płyta CD), jednak nie da się jej wprowadzić jako tekst w odpowiednie pole ani poprzez wgrywanie pliku.",
        'otherMethod' : "Tę opcję należy wybrać jeżeli treść tłumaczenia jest nie jest możliwa do wprowadzenia przy wykorzystaniu żadnej z dwóch pozostałych metod.",
        'otherMethodClicked' : "W celu wyceny tłumaczenia prosimy o przesłanie szczegółów zlecenia na adres e-mail: kontakt@aworia.pl lub poprzez formularz kontaktowy.",

        'inputLanguage' : "Należy podać język tekstu źródłowego. Jeśli nie potrafisz go określić, wybierz opcję nieznany.",

        'textOrFiles' : "Należy wprowadzić tekst źródłowy i/lub załączyć plik z treścią. Aby wpisać tekst, można użyć skrótów klawiszowych ctrl+c (kopiuj) oraz ctrl+v (wklej). ",
        'transType' : "Przy tłumaczeniu zwykłym nie jest wymagane poświadczenie zgodności treści z oryginałem. Tego typu usługa najczęściej występuje w sprawach nieurzędowych. Wówczas strona rozliczeniowa wynosi 1500 znaków ze spacjami. Tłumaczenie przysięgłe (uwierzytelnione) wykonuje się, gdy niezbędne jest poświadczenie treści dokumentu z oryginałem. Taką usługę może sporządzić jedynie tłumacz danego języka, posiadający stosowne uprawnienia. Tłumaczenie uwierzytelnione najczęściej ma miejsce w przypadku dokumentów wykorzystywanych do spraw urzędowych. Strona rozliczeniowa wynosi wówczas 1125 znaków ze spacjami. Jeśli nie jesteś pewien jakiego typu tłumaczenia potrzebujesz, zaznacz opcję “Nie wiem”. W takim przypadku, w polu informacji dla tłumacza należy wyjaśnić do czego potrzebne jest tłumaczenie. Tłumacz zweryfikuje podane informacje i sam wybierze odpowiednią opcję. ",
        'transSpecial' : "Tłumaczenie specjalistyczne występuje, gdy do przekładu tekstu potrzebna jest specjalistyczna wiedza z danej dziedziny. Treść tego typu zawiera zwykle nietypowe sformułowania nieznane powszechnej grupie odbiorców. Mogą to być m.in. teksty medyczne, techniczne i prawnicze. Jeśli nie jesteś pewien, czy Twoje tłumaczenie należy do tej grupy zaznacz Nie wiem. Tłumacz zweryfikuje przesłany tekst i wybierze odpowiednią opcję.",
        'term' : "Przedstawione terminy liczone są od momentu wykonania i zarejestrowania wpłaty przez operatora szybkich płatności internetowych oraz z założeniem, że zlecenie zostało poprawnie wprowadzone i nie wymaga dodatkowych wyjaśnień. ",
        'infoForTranslator' : "Należy wprowadzić tekst źródłowy i/lub załączyć plik z treścią. Aby wpisać tekst, można użyć skrótów klawiszowych ctrl+c (kopiuj) oraz ctrl+v (wklej). ",
        'deliveryMethod' : "Istnieje możliwość wyboru dwóch sposobów dostawy równocześnie. W przypadku tłumaczenia przysięgłego nie ma możliwości zrezygnowania z dostawy na adres korespondencyjny.",
        'deliveryType' : "Istnieje możliwość wyboru dwóch sposobów dostawy równocześnie. W przypadku tłumaczenia przysięgłego nie ma możliwości zrezygnowania z dostawy na adres korespondencyjny.",

        'cennikTranslationOne' : "Przy tłumaczeniu zwykłym nie jest wymagane poświadczenie zgodności treści z oryginałem. Tego typu usługa najczęściej występuje w sprawach nieurzędowych. Wówczas strona rozliczeniowa wynosi 1500 znaków ze spacjami.",
        'cennikTranslationTwo': "Tłumaczenie przysięgłe (uwierzytelnione) wykonuje się, gdy niezbędne jest poświadczenie treści dokumentu z oryginałem. Taką usługę może sporządzić jedynie tłumacz danego języka, posiadający stosowne uprawnienia. Tłumaczenie uwierzytelnione najczęściej ma miejsce w przypadku dokumentów wykorzystywanych do spraw urzędowych. Strona rozliczeniowa wynosi wówczas 1125 znaków ze spacjami.",
        'discount' : "Rabaty są przyznawane za każdym razem automatycznie gdy zlecenie spełnia określone wymagania ilościowe tekstu.",
        'timeRealization': "Przedstawione terminy liczone są od momentu zarejestrowania wpłaty na naszym koncie bankowym oraz z założeniem, że zlecenie zostało poprawnie wprowadzone i nie wymaga dodatkowych wyjaśnień.",

        'formGood': "Formularz został pomyślnie wysłany.",

        'payment': "Dziękujemy zaskorzystanie z naszych usług. Serwis oczekuje na wiadomość potwierdzającąuregulowanie płatności. Będziemy mogli zrealizować Twoje zamówienie, gdyoperator szybkich płatności powiadomi nas o dokonanej wpłacie.Jeśli z dowolnego powodu wywnioskujesz, że wcześniej zdefiniowana wpłata niezostała sfinalizowana, możesz dokonać płatności ponownie. W przypadku odnotowania przez nas nadpłaty, różnica kwot zostanie Ci zwrócona.",
        'letTranslation': "Dziękujemy za przesłanie formularza. Twoje zlecenie trafiło do tłumacza, który wkrótce je wyceni. ",

        'cennikSpecialisticTranslation': "Tłumaczenie specjalistyczne występuje, gdy do przekładu tekstu potrzebna jest specjalistyczna wiedza z danej dziedziny. Treść tego typu zawiera zwykle nietypowe sformułowania nieznane powszechnej grupie odbiorców. Mogą to być m.in. teksty medyczne, techniczne i prawnicze.",
        'noticeFalse': "Uzupełnij pominięte pola.",
}

const state = {
    visible: false,
    textTag: '',
    texts: texts,
    link: false,
    linkDestination: "",
    linkDescription: "",

    /* Fullscreen modal */
    fullscreenVisible: false,
    loading: true,
    fullscreenLogs: [],
    paymentActive: false,
    linkToPayment: "#",
    loaderClasses: ""
}

const mutations = {
    openModal(state, payload){
        state.visible = true;
        state.textTag = payload;
    },
    closeModal(state){
        state.visible = false;
        state.textTag = "";
        state.link = false;
        state.linkDestination = "";
        state.linkDescription = "";
    },
    setButton(state, payload){
        state.link = true;
        state.linkDestination = payload[0];
        state.linkDescription = payload[1];
    },


    /* Fullscreen */
    openFullscreen(state){
        state.fullscreenVisible = true;
    },
    addLogFullscreen(state, payload){
        state.fullscreenLogs.push(payload);
    },
    addPayment(state, payload){
        state.linkToPayment = payload;
        state.paymentActive = true;
    },
    stopLoading(state){
        state.loaderClasses = "fadeOutUp"
        state.loading = false;
    }
}

const getters = {

}

const namespaced = true;

export const modal = {
    state,
    mutations,
    namespaced,
    getters
}

