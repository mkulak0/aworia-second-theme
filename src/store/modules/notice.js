import router from '../../router/index';

const state = {
    noticesFirst: {
        languageInput: false,
        languageOutput: false,
        textOrFiles: false, 
        transType: false,
        transSpecial: false,
    },
    noticesSecond: {
        /* Dane zamawiającego */
        phone: false,

        email: false,
        emailsEqual: false,

        /* Sposób dostawy */
        oneDelivery: false,

        name: false,
        address: false,
        postCode: false,
        city: false,
        phone2: false,
        country: false,

        billName: false,
        billAddress: false,
        billPostCode: false,
        billCity: false,
        billPhone: false,
        billCountry: false,

        statute: false,
        rodo: false,
    }
}

const mutations = {
    resetFirst(state){
        state.noticesFirst.forEach((value, key) => {
            state.noticesFirst[key] = false;
        })
    },
    changeFirst(state, payload){
        state.noticesFirst[payload] = true;
    },

    resetSecond(state){
        state.noticesSecond.forEach((value, key) => {
            state.noticesSecond[key] = false;
        })
    },
    changeSecond(state, payload){
        state.noticesSecond[payload] = true;
    }
}

const actions = {
    checkFirst({rootState, commit, dispatch, rootGetters}){
        commit("resetFirst");
        if(rootState.languages.selectedInput == 0){
            commit("changeFirst", "languageInput");
        }
        if(rootState.languages.selectedOutput.length == 0){
            commit("changeFirst", "languageOutput");
        }
        if(rootGetters['form/fileListLength'] == 0 && rootState.form.text == ""){
            commit("changeFirst", "textOrFiles");
        }
        if(rootState.form.transType == "none"){
            commit("changeFirst", "transType");
        }
        if(rootState.form.transSpecial == "none"){
            commit("changeFirst", "transSpecial");
        }
    },
    checkSecond({rootState, commit, dispatch, rootGetters}){
        commit("resetSecond");
        /* Dane  zamawiającego */
        if(rootState.post.clientPhone == ""){
            commit("changeSecond", "phone");
        }
        if(rootState.post.clientEmail == ""){
            commit("changeSecond", "email");
        }
        if(rootState.post.clientEmail != rootState.post.clientEmailCheck){
            commit("changeSecond", "emailsEqual");
        }

        /* Sposób dostawy */
        if(
            !(
                rootState.post.delivery.byEmail == true || 
                rootState.post.delivery.byPrivate == true || 
                rootState.post.delivery.byCompany == true
            )
        ){
            commit("changeSecond", "oneDelivery");
        }

        /* Sprawdzaj tylko gdy wybrano te opcje dostawy */
        if(rootState.post.delivery.byPrivate == true || rootState.post.delivery.byCompany == true){

            if(rootState.post.delivery.name == ""){
                commit("changeSecond", "name");
            }
            if(rootState.post.delivery.address == ""){
                commit("changeSecond", "address");
            }
            if(rootState.post.delivery.postCode == ""){
                commit("changeSecond", "postCode");
            }
            if(rootState.post.delivery.city == ""){
                commit("changeSecond", "city");
            }
            if(rootState.post.delivery.phone == ""){
                commit("changeSecond", "phone2");
            }
            if(rootState.post.delivery.country == ""){
                commit("changeSecond", "country");
            }
        }

        if(rootState.post.bill){
            if(rootState.post.billValues.name == ""){
                commit("changeSecond", "billName");
            }
            if(rootState.post.billValues.address == ""){
                commit("changeSecond", "billAddress");
            }
            if(rootState.post.billValues.postCode == ""){
                commit("changeSecond", "billPostCode");
            }
            if(rootState.post.billValues.city == ""){
                commit("changeSecond", "billCity");
            }
            if(rootState.post.billValues.phone == ""){
                commit("changeSecond", "billPhone");
            }
            if(rootState.post.billValues.country == ""){
                commit("changeSecond", "billCountry");
            }
        }

        if(rootState.form.rodo == false){
            commit("changeSecond", "rodo");
        }
        if(rootState.form.statute == false){
            commit("changeSecond", "statute");
        }
    }

}

const getters = {
    noticesFirst: state => () => {
        return state.noticesFirst;
    },
    isValid: state => () => {
        let output = true;
        state.noticesFirst.forEach(value => {
            if(output == true){
                if(value == true){
                    output = false;
                }
            }
        });

        state.noticesSecond.forEach(value => {
            if(output == true){
                if(value == true){
                    output = false;
                }
            }
        });

        return output;
    }
}

const namespaced = true;

export const notice = {
    namespaced, state, actions, mutations, getters
}