import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

//import createPersist from 'vuex-localstorage'
/* To wstawia zawartość Vuexa do localStorage, może się przyda */
/* let localStorage = createPersist({
    namespace: 'YOUR_APP_NAMESPACE',
    initialState: {},
    expires: 1.21e+9 // Two Weeks
}) 
//plugins: [localStorage]
*/

import * as actions from './actions'
import * as getters from './getters'

import { wycena } from './modules/wycena'
import { modal } from './modules/modal'
import { languages } from './modules/languages'
import { form } from './modules/form'
import { post } from './modules/post'
import { send } from './modules/send'
import { notice } from './modules/notice'



const debug = process.env.NODE_ENV !== 'production'

const state = {
  reload: false
}

const mutations = {
  changeReload(state){
    state.reload = !state.reload;
  }
}


export default new Vuex.Store({
  actions,
  getters,
  state,
  mutations,
  modules: {
    wycena,
    modal,
    languages,
    form,
    post,
    send,
    notice,
  },
  strict: debug,
  
})
