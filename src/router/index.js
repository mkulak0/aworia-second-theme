import Vue from 'vue';
import Router from 'vue-router';

// Components
import Home from '../components/Home.vue';
import Wycena from '../components/Wycena.vue'
import Wycena2 from '../components/Wycena2.vue'
import Cennik from '../components/Cennik';
import Form from '../components/Form'

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/wycena',
      name: 'Wycena',
      component: Wycena
    },
    {
      path: '/wycena2',
      name: "Wycena2",
      component: Wycena2
    },
    {
      path: '/cennik',
      name: 'Cennik',
      component: Cennik
    },
    {
      path: '/form',
      name: "Formularz",
      component: Form
    }
  ],
  mode: 'history',
  base: '/', //production
  //base: '/aworia', //dev

  // Prevents window from scrolling back to top
  // when navigating between components/views
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  },
});

router.afterEach((to) => { // (to, from)
  // Add a body class specific to the route we're viewing
  let body = document.querySelector('body');

  const slug = !(to.params.postSlug)
    ? to.params.pageSlug
    : to.params.postSlug;
  body.classList.add('vue--page--' + slug);
});

export default router;
