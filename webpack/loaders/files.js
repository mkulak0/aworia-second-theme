
module.exports = {
  test: /\.(svg|jpg|gif|jpeg)$/,
  use: [
    {
      loader: 'file-loader',
      options: {
          name: 'img/[name].[ext]',
      },
    },
  ],
};
