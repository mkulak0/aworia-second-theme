const CopyPlugin = require('copy-webpack-plugin');

module.exports = new CopyPlugin([
    {
        from: 'src/assets/*.png',
        to: 'img/[name].[ext]',
    }
]);