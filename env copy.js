module.exports = {
    url: 'localhost/aworia/',
    themeDirectory: 'wp-content/themes/aworia-second-theme/dist/',
    api: 'wp-json/wp/v2/'
}